import hotelsClient from '../services/hotelsClient';
import bunyanLog from '../services/bunyanLog';
import availabilityDecorator from '../services/availabilityDecorator';
import blockAvailabilityDecorator from '../services/blockAvailabilityDecorator';
import _ from 'lodash';

function mapToListFormat(docs) {
  return _.map(docs, (doc) => {
    return doc; //TODO: filter only necesary props for a list
  });
}

function buildFiltersObject(req) {

  let filters = {};

  filters['location.city.poiId'] = req.params.poi;

  if (req.params.minPrice && parseInt(req.params.minPrice) > 0) {
    filters['rates.min'] = { '$gte': parseInt(req.params.minPrice) };
  }

  if (req.params.maxPrice && parseInt(req.params.maxPrice) < 1500) {
    filters['rates.max'] = { '$lte': parseInt(req.params.maxPrice) };
  }

  if (req.params.minStars && parseFloat(req.params.minStars) > 1) {
    filters['stars'] = { '$gte': parseFloat(req.params.minStars) };
  }

  if (req.params.minScore && parseInt(req.params.minScore) > 0) {
    filters['statistics.reviewScore'] = { '$gte': parseInt(req.params.minScore) };
  }

  if (req.params.maxScore && parseInt(req.params.maxScore) < 10) {
    filters['statistics.reviewScore'] = { '$lte': parseInt(req.params.maxScore) };
  }

  return filters;

}

function buildSortObject(req) {

  switch (req.params.sort) {
    case 'price': return { 'rates.min': 1 };
    case 'stars': return { 'stars': -1 };
    case 'score': return { 'statistics.reviewScore': -1 };
    default: return { '_id': 1 };
  }

}

export function hotelsController (server) {

  server.get('/hotels/featured', function(req, res, next) {

    hotelsClient
      .queryAllHotels({
        language: req.params.language || 'es',
        filters: {
          country: req.params.country || 'mx',
          'statistics.reviewScore': { '$gt': 7 }
        },
        page: req.params.page || 1,
        limit: req.params.limit || 10
      })
      .then((answer) => {

        let currency = req.params.currency || 'MXN';

        return availabilityDecorator('2016-08-20', '2016-08-23', answer.payload, currency);

      })
      .then((hotels) => {

        res.cache('public', { maxAge: 3600 });

        switch (req.params.format) {
          case 'list':
            res.send(mapToListFormat(hotels));
          default:
            res.send(hotels);
        }

        next();

      })
      .catch((err) => {

        bunyanLog.error(err);

        res.error(err);
        next();

      });

  });

  server.get('hotels/stats', (req, res, next) => {

    hotelsClient
      .queryAllHotels({
        language: req.params.language || 'es',
        poiId: req.params.poi,
        page: req.params.page || 1,
        limit: req.params.limit || 10,
        filters: buildFiltersObject(req)
      })
      .then((answer) => {

        res.cache('public', { maxAge: 3600 });

        res.send({
          itemCount: answer.itemCount,
          pageCount: answer.pageCount
        });

        next();

      });

  });

  server.get('hotels', (req, res, next) => {

    hotelsClient
      .queryAllHotels({
        language: req.params.language || 'es',
        page: req.params.page || 1,
        limit: req.params.limit || 10,
        filters: buildFiltersObject(req),
        sort: buildSortObject(req)
      })
      .then((answer) => {

        if (req.params.arrival && req.params.departure) {

          return availabilityDecorator(req.params.arrival, req.params.departure, answer.payload, req.params.currency);

        } else {

          return answer.payload;

        }

      })
      .then((hotels) => {

        res.cache('public', { maxAge: 3600 });

        switch (req.params.format) {
          case 'list':
            res.send(mapToListFormat(hotels));
          default:
            res.send(hotels);
        }

        next();

      })
      .catch((err) => {

        bunyanLog.error(err);

        res.error(err);
        next();

      });


  });

  server.get('hotels/:id', (req, res, next) => {

    hotelsClient
      .queryHotelById({
        id: req.params.id
      })
      .then((answer) => {

        bunyanLog.info('received answer from hotel client', answer);

        return answer.payload;

      })
      .then((hotel) => {

        if (hotel && req.params.arrival && req.params.departure) {

          bunyanLog.info('decorating hotel with block availability', hotel);

          return blockAvailabilityDecorator(req.params.arrival, req.params.departure, hotel, req.params.currency);

        } else {

          return hotel;

        }

      })
      .then((hotel) => {

        bunyanLog.info('sending hotel object', hotel);

        res.cache('public', { maxAge: 3600 });

        if (hotel) {
          res.send(hotel);
          next();
        } else {
          res.send(404, 'Hotel not found');
          next();
        }

      })
      .catch((err) => {

        bunyanLog.error(err);

        res.error(err);
        next();

      });


    });

}
