import flightsClient from '../services/flightsClient';
import mappings from '../services/mappingObjects.js';
import bunyanLog from '../services/bunyanLog';
import _ from 'lodash';

function mapToListFormat(docs) {
  return _.map(docs, (doc) => {
    return doc; //TODO: filter only necesary props for a list
  });
}

export function flightsController (server) {

  server.get('/flights', function(req, res, next) {

    flightsClient
      .queryAvailability({
        page: req.params.page || 1,
        limit: req.params.limit || 10,
        itinerary: req.params.itinerary,
        paxs: req.params.paxs,
        cabin_type: req.params.cabin_type || null
      })
      .then((flights) => {

        //res.cache('public', { maxAge: 3600 });

        switch (req.params.format) {
          case 'list':
            res.send(mapToListFormat(flights));
          default:
            res.send(mappings.mapToFlightData(flights));
        }

        next();

      })
      .catch((err) => {

        bunyanLog.error(err);

        res.send({Error: err });
        next();

      });

  });

}
