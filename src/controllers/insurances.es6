//import insurancesClient from '../services/insurancesClient'
import bunyanLog from '../services/bunyanLog';
import _ from 'lodash';

function mapToListFormat(docs) {
  return _.map(docs, (doc) => {
    return doc; //TODO: filter only necesary props for a list
  });
}

export function insurancesController (server) {

  server.get('/insurances', function(req, res, next) {

    var insurances = [
        {
            plan: "Plan Basic",
            info: "(hasta 64 años)",
            items: [
                { "id":1, "expiration": 3, "location": "Venecia", "image": "http://q-ec.bstatic.com/data/explorer_city/max800/105/10576.jpg", "name":"Trip Protection", "currency":"USD", "price":120 },
                { "id":2, "expiration": 4, "location": "Nueva York", "image": "http://r-ec.bstatic.com/data/explorer_city/max800/142/1427.jpg", "name":"Medical Coverage", "currency":"USD", "price":340 },
                { "id":3, "expiration": 2, "location": "Roma", "image": "http://q-ec.bstatic.com/data/explorer_city/max800/183/1838.jpg", "name":"Luggage", "currency":"EU", "price":40 } 
            ]
        },
        {
            plan: "Plan Premium",
            info: "(hasta 100 años)",
            items: [
                { "id":4, "expiration": 2, "location": "Madrid", "image": "http://q-ec.bstatic.com/data/explorer_city/max800/101/1011.jpg", "name":"Trip Protection 2", "currency":"USD", "price":240 },
                { "id":5, "expiration": 4, "location": "Londres", "image": "http://q-ec.bstatic.com/data/explorer_city/max800/877/877.jpg", "name":"Medical Coverage 2", "currency":"USD", "price":960 },
                { "id":6, "expiration": 2, "location": "Dubai", "image": "http://r-ec.bstatic.com/data/explorer_city/max800/175/1756.jpg", "name":"Luggage 2", "currency":"EU", "price":60 } 
            ]
        }
    ];

    bunyanLog.info('Loggin insurance', JSON.stringify(insurances)); 
    res.send(insurances);

  });

  server.get('/insurances/featured', function(req, res, next) {

    var featured = [
        {
            plan: "Plan Basic",
            info: "(hasta 64 años)",
            items: [
                { "id":7, "expiration": 3, "location": "Venecia", "image": "http://q-ec.bstatic.com/data/explorer_city/max800/105/10576.jpg", "name":"Trip Protection", "currency":"USD", "price":120 },
                { "id":8, "expiration": 4, "location": "Nueva York", "image": "http://r-ec.bstatic.com/data/explorer_city/max800/142/1427.jpg", "name":"Medical Coverage", "currency":"USD", "price":340 },
                { "id":9, "expiration": 2, "location": "Roma", "image": "http://q-ec.bstatic.com/data/explorer_city/max800/183/1838.jpg", "name":"Luggage", "currency":"EU", "price":40 } 
            ]
        },
        {
            plan: "Plan Premium",
            info: "(hasta 100 años)",
            items: [
                { "id":10, "expiration": 2, "location": "Madrid", "image": "http://q-ec.bstatic.com/data/explorer_city/max800/101/1011.jpg", "name":"Trip Protection 2", "currency":"USD", "price":240 },
                { "id":11, "expiration": 4, "location": "Londres", "image": "http://q-ec.bstatic.com/data/explorer_city/max800/877/877.jpg", "name":"Medical Coverage 2", "currency":"USD", "price":960 },
                { "id":12, "expiration": 2, "location": "Dubai", "image": "http://r-ec.bstatic.com/data/explorer_city/max800/175/1756.jpg", "name":"Luggage 2", "currency":"EU", "price":60 } 
            ]
        }
    ];

    bunyanLog.info('Loggin featured insurance', JSON.stringify(featured)); 
    res.send(featured);

  });

  server.get('/insurances/:id', function(req, res, next) {

    switch (req.params.id) {
      case '3':
        
        // var insurances={"insurances":[
        //         {"id":1, "name":"Trip Protection", "currency":"USD", "price":120},            
        //         {"id":2, "name":"Medical Coverage", "currency":"USD", "price":340},
        //         {"id":3, "name":"Luggage", "currency":"EU", "price":20}]};
        // res.send(insurances.insurances[0].id);

        var insurance_by_id={"id":3, "name":"Car Rental", "currency":"USD", "price":120};
        res.send(insurance_by_id);

        bunyanLog.info('Loggin insurance by id', JSON.stringify(req.params.id));
      default:
        res.send('');
    }

    next();

    // insurancesClient
    //   .queryInsuranceById({
    //     id: req.params.id || null
    //   })
    //   .then((insurances) => {

    //     res.cache('public', { maxAge: 3600 });

    //     switch (req.params.format) {
    //       case 'list':
    //         res.send(mapToListFormat(insurances));
    //       default:
    //         res.send(insurances);
    //     }

    //     bunyanLog.info('loggin insurance', JSON.stringify(result));
    //     next();

    //   })
    //   .catch((err) => {

    //     bunyanLog.error(err);

    //     res.error(err);
    //     next();

    //   });

  });

}
