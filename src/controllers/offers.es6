import offersClient from '../services/offersClient';
import bunyanLog from '../services/bunyanLog';
import availabilityDecorator from '../services/availabilityDecorator';
import _ from 'lodash';

function mapToListFormat(docs) {
  return _.map(docs, (doc) => {
    return doc; //TODO: filter only necesary props for a list
  });
}

export function offersController (server) {

  server.get('/offers', function(req, res, next) {

    offersClient
      .queryOffers({
        country: req.params.country || 'mx',
        isAlert: req.params.isAlert || false,
        page: req.params.page || 1,
        limit: req.params.limit || 10
      })
      .then((offers) => {

        res.cache('public', { maxAge: 3600 });

        switch (req.params.format) {
          case 'list':
            res.send(mapToListFormat(offers));
          default:
            res.send(offers);
        }

        next();

      })
      .catch((err) => {

        bunyanLog.error(err);

        res.error(err);
        next();

      });

  });

  server.get('/offers/:id', function(req, res, next) {

    offersClient
      .queryOfferById({
        id: req.params.id || null
      })
      .then((offers) => {

        res.cache('public', { maxAge: 3600 });

        switch (req.params.format) {
          case 'list':
            res.send(mapToListFormat(offers));
          default:
            res.send(offers);
        }

        next();

      })
      .catch((err) => {

        bunyanLog.error(err);

        res.error(err);
        next();

      });

  });

}
