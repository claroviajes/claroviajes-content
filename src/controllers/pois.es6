import poisClient from '../services/poisClient';
import flightsClient from '../services/flightsClient';
import mappings from '../services/mappingObjects.js';
import bunyanLog from '../services/bunyanLog';
import _ from 'lodash';

function mapToTypeaheadFormat(docs) {
  return _.map(docs, (doc) => {
    return {
      _id: doc._id,
      name: doc.name,
      country: doc.country,
      hotelCount: doc.stats.hotelCount
    }
  });
}

export function poisController (server) {

  server.get('pois', (req, res, next) => {

    poisClient
      .queryMatchingPois({
        //HACK: avoiding language filter to test data consistency
        //language: req.params.language || 'es',
        hint: req.params.hint,
        limit: req.params.limit || 10
      })
      .then((answer) => {

        switch (req.params.format) {
          case 'typeahead':
            res.send(mapToTypeaheadFormat(answer.payload));
          default:
            res.send(answer.payload);
        }

        next();

      })
      .catch((err) => {

        bunyanLog.error(err);

        res.error(err);
        next();

      });

  });

  server.get('pois/iatas', (req, res, next) => {

    flightsClient
      .queryZones({
        //HACK: avoiding language filter to test data consistency
        hint: req.params.hint,
        limit: req.params.limit || 10
      })
      .then((answer) => {

        res.send(mappings.mapToPoisIata(answer));

        next();

      })
      .catch((err) => {

        bunyanLog.error(err);

        res.send({Error: err });
        next();

      });

  });

}
