import PoisClient from 'claroviajes-pois-client';

let singleton = new PoisClient({
  mongoUrl: process.env.POIS_MONGO_URL
});

export default singleton
