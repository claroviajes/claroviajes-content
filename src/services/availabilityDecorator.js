import bookingcomClient from './bookingcomClient';
import bunyanLog from './bunyanLog';
import _ from 'lodash';

function extractHotelIds(hotels) {

  return _.map(hotels, (h) => {
    return h.handles.bookingcom;
  });

}

function getBestCurrencyMatch(hotelAvailability) {

  if (hotelAvailability.other_currency != null && hotelAvailability.other_currency.length > 0) {

    return hotelAvailability.other_currency[0];

  } else {

    return hotelAvailability;

  }

}

function mergeSets(hotels, availability) {

  return _.map(hotels, (h) => {

    let hotelMatch = _.find(availability, (a) => a.hotel_id == h.handles.bookingcom);

    if (!hotelMatch) return h;

    let currencyMatch = getBestCurrencyMatch(hotelMatch);

    h.availability = {
      currency: currencyMatch.currency_code,
      totalRate: { min: currencyMatch.min_total_price, max: currencyMatch.max_total_price },
      perNightRate: { min: currencyMatch.min_price, max: currencyMatch.max_price }
    };

    return h;

  });

}

export default function (arrival, departure, hotels, currency) {

  return new Promise((resolve, reject) => {

    let apiQs = {
      arrival_date: arrival,
      departure_date: departure,
      hotel_ids: extractHotelIds(hotels),
      currency_code: currency
    };

    bunyanLog.info('calling booking api for availability', apiQs);

    bookingcomClient
      .getHotelAvailability(apiQs)
      .then((availability) => {

        bunyanLog.info('booking api availabilty received', availability)

        return mergeSets(hotels, availability);

      })
      .then((mergedData) => {

        resolve(mergedData);

      })
      .catch((err) => {

        bunyanLog.error(err, 'error while decorating availability, returning original hotel set');

        resolve(hotels);

      });

  });

}
