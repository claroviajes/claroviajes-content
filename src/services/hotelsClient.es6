import HotelsClient from 'claroviajes-hotels-client';

let singleton = new HotelsClient({
  mongoUrl: process.env.HOTELS_MONGO_URL
});

export default singleton
