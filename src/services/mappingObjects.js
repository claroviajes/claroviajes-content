import _ from 'lodash';
import bunyanLog from './bunyanLog';

exports.mapToPoisIata = function(data){
  var newResult = _.map(data, function(e){
    return {
      code: e.codigo,
      name: e.nombre,
      iata: e.iata
    }
  });
  return newResult;
}

function mapToFlightSegment(data) {

  try{
    return {
      departureAirport: {
        locationCode: data.DepartureAirport.LocationCode,
        locationName: data.DepartureAirport.LocationName
      },
      arrivalAirport:{
        locationCode: data.ArrivalAirport.LocationCode,
        locationName: data.ArrivalAirport.LocationName
      },
      operatingAirline: data.OperatingAirline ? {
         code: data.OperatingAirline.Code,
         name: data.OperatingAirline.Name,
         url: data.OperatingAirline.url
       } : null,
      equipment:{
        airEquipType: data.Equipment.AirEquipType
      },
      marketingAriline: data.MarketingAirline ? {
        code: data.MarketingAirline.Code,
        name:data.MarketingAirline.Name,
        url:data.MarketingAirline.url
      } : null,
      marriageGrp: data.MarriageGrp,
      departureDateTime: data.DepartureDateTime,
      arrivalDateTime: data.ArrivalDateTime,
      stopQuantity: data.StopQuantity,
      rph: data.RPH,
      flightNumber: data.FlightNumber,
      status: data.Status,
      durationStep: data.DuracionTramo,
      acumulateDuration: data.TiempoAcumulado

    }
  }
  catch(err)
  {
    bunyanLog.error(err, data);
    return data;
  }
}

exports.mapToFlightData = function(data){

    var newResult = [];

    for (var i = 0; i < Object.keys(data).length-1; i++) {
      var currentItem = data[i];

      var outGoFlight = null;
      var returnFlight = null;
//OutgoFlight
      if (currentItem.AirItinerary[0]!=null){
        outGoFlight = {
          flightSegments: _.map(currentItem.AirItinerary[0].FlightSegment, function(s){
            return mapToFlightSegment(s);
          }),
          refNumber: currentItem.AirItinerary[0].RefNumber,
          totalFlightDuration: currentItem.AirItinerary[0].DuracionTotalVuelo
        }
      }
      //ReturnFlight
      if (currentItem.AirItinerary[1]!=null){
        returnFlight = {
          flightSegments: _.map(currentItem.AirItinerary[1].FlightSegment, function(s){
            return mapToFlightSegment(s);
          }),
          refNumber: currentItem.AirItinerary[1].RefNumber,
          totalFlightDuration: currentItem.AirItinerary[1].DuracionTotalVuelo
        }
      }

      var _PtcFares =   _.map(currentItem.AirItineraryPricingInfo.PTC_FareBreakdowns.PTC_FareBreakdown, function(f){
          return {
            passengerTypeQuantity:{
              code: f.PassengerTypeQuantity.Code,
              quantity: f.PassengerTypeQuantity.Quantity
            },
            passengerFare: {
              baseFare:{
                amount: f.PassengerFare.BaseFare.Amount,
                currencyCode: f.PassengerFare.BaseFare.CurrencyCode,
                decimalPlaces: f.PassengerFare.BaseFare.DecimalPlaces
              },
              taxes: {
                tax:{
                  amount: f.PassengerFare.Taxes.Tax.Amount,
                  currencyCode: f.PassengerFare.Taxes.Tax.CurrencyCode,
                  taxCode: f.PassengerFare.Taxes.Tax.TaxCode
                }
              },
              totalFare: {
                amount: f.PassengerFare.TotalFare.Amount,
                currencyCode: f.PassengerFare.TotalFare.CurrencyCode,
                decimalPlaces: f.PassengerFare.TotalFare.DecimalPlaces
              }
            }
          }
        });

      newResult.push({
        airItinerary: {
          outGo: outGoFlight,
          return: returnFlight
        },
        airItineraryPricingInfo: {
          itinTotalFare: {
            totalFare:{
              amount: currentItem.AirItineraryPricingInfo.ItinTotalFare.TotalFare.Amount,
              currencyCode: currentItem.AirItineraryPricingInfo.ItinTotalFare.TotalFare.CurrencyCode,
              decimalPlaces: currentItem.AirItineraryPricingInfo.ItinTotalFare.TotalFare.DecimalPlaces
            },
            tpaExtensions:{
              flightId:  currentItem.AirItineraryPricingInfo.ItinTotalFare.TPA_Extensions.idVuelo
            },
            totalNeto:{
              amount: currentItem.AirItineraryPricingInfo.ItinTotalFare.TotalNeto.Amount,
              currencyCode: currentItem.AirItineraryPricingInfo.ItinTotalFare.TotalNeto.CurrencyCode,
              decimalPlaces: currentItem.AirItineraryPricingInfo.ItinTotalFare.TotalNeto.DecimalPlaces
            }
          },
          ptcFareBreakdowns: _PtcFares

        },
        directionInd: currentItem.DirectionInd,
        sourceInfo: currentItem.fuente_de_informacion,
        sequenceNmbr: data.SequenceNmbr
      });


    }

  return newResult;
}
