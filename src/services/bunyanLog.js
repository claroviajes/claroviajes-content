import bunyan from 'bunyan';

let log = bunyan.createLogger({
  name        : 'claroviajes-content',
  level       : process.env.LOG_LEVEL || 'info',
  stream      : process.stdout,
  serializers : bunyan.stdSerializers
});

export default log
