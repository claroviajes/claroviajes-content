import OffersClient from 'claroviajes-offers-client';

let singleton = new OffersClient({
  mongoUrl: process.env.OFFERS_MONGO_URL
});

export default singleton
