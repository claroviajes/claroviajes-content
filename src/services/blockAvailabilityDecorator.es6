import bookingcomClient from './bookingcomClient';
import bunyanLog from './bunyanLog';
import _ from 'lodash';

export default function (arrival, departure, hotel, currency) {

  return new Promise((resolve, reject) => {

    let apiQs = {
      arrival_date: arrival,
      departure_date: departure,
      hotel_ids: [ hotel.handles.bookingcom ],
      currency_code: currency,
      detail_level: 1
    };

    bunyanLog.info('calling booking api for block availability', apiQs);

    bookingcomClient
      .getBlockAvailability(apiQs)
      .then((availability) => {

        bunyanLog.info('booking api block availability received', availability)

        if (availability.length > 0) {
          hotel.availability = availability[0].block;
        }

        return hotel;

      })
      .then((mergedData) => {

        resolve(mergedData);

      })
      .catch((err) => {

        bunyanLog.error(err, 'error while decorating block availability, returning original hotel');

        resolve(hotel);

      });

  });

}
