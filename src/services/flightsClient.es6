import bunyanLog from './bunyanLog';

var Client = require('node-rest-client').Client;

var options_auth={user: process.env.FALCON_API_USERNAME, password: process.env.FALCON_API_PASSWORD};

exports.queryZones = function(data){
  return new Promise((resolve, reject) => {
    var client = new Client(options_auth);
    var params = {consulta: data.hint, aereos:1};
    var url = process.env.FALCON_API_URL + "/zonas/busca.json";
    var args ={
        parameters:params
      };

    client.get(url, args,
        function(data, response){
              bunyanLog.info('Response from falcon');

              var data = JSON.parse(data.toString())
              if (data.Error)
              {
                bunyanLog.error(data.Error);
                reject(data.Error);
              }
              else {
                data = data.zonas.busca;
                resolve(data);
              }

            // raw response
            //console.log(response);
    }).on('error',function(err){
            bunyanLog.error(err);
            reject(err);
        });
  });
}

exports.queryAvailability = function(data){
  return new Promise((resolve, reject) => {
    var client = new Client(options_auth);
    var params = {tramos: data.itinerary, pasajeros: data.paxs, tipo_cabina: data.cabin_type};
    var url = process.env.FALCON_API_URL + "/aereos/disponibilidad.json";
    var args ={
        parameters:params
      };

    client.get(url, args,
        function(data, response){
            bunyanLog.info('Response from falcon');
            bunyanLog.info(url);

            var data = JSON.parse(data.toString())
            if (data.Error)
            {
              bunyanLog.error(data.Error);
              reject(data.Error);
            }
            else {
              data = data.aereos.disponibilidad;
              resolve(data);
            }

    }).on('error',function(err){
            bunyanLog.error(err);
            reject(err);
        });
  });
}
