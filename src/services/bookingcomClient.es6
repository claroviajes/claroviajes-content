import BookingApiClient from 'bookingcom-client';

let singleton = new BookingApiClient({
  username: process.env.BOOKING_API_USERNAME,
  password: process.env.BOOKING_API_PASSWORD,
  testMode: process.env.BOOKING_TEST_MODE || 'FULL'
});

export default singleton;
