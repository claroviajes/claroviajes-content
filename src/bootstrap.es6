import restify from 'restify';
import bunyanLog from './services/bunyanLog';
import poisClient from './services/poisClient';
import hotelsClient from './services/hotelsClient';
import offersClient from './services/offersClient';

import * as controllers from './controllers';

let server = restify.createServer({
  name : 'claroviajes-content',
  log  : bunyanLog
});

server.pre(restify.pre.sanitizePath());
server.use(restify.acceptParser(server.acceptable));
server.use(restify.bodyParser({ mapParams: false }));
server.use(restify.queryParser());
server.use(restify.gzipResponse());
server.use(restify.CORS());
server.on('after', restify.auditLogger({ log: bunyanLog }));

controllers.offersController(server);
controllers.hotelsController(server);
controllers.poisController(server);
controllers.insurancesController(server);
controllers.flightsController(server);

export default { bunyanLog, poisClient, hotelsClient, offersClient, server }
