'use strict';

import dotenv from 'dotenv';
dotenv.load();

//HACK: use require instead of import to ensure it's run after dotenv load
let bootstrap = require('./bootstrap');

const PORT = process.env.PORT || 8080;

bootstrap.poisClient.connect().catch(() => {
  bunyanLog.error('pois client error');
});

bootstrap.hotelsClient.connect().catch(() => {
  bunyanLog.error('hotels client error');
});

bootstrap.offersClient.connect().catch(() => {
  bunyanLog.error('offers client error');
});

bootstrap.bunyanLog.info('server starting', { port: PORT });

bootstrap.server.listen(PORT, function () {
  bootstrap.bunyanLog.info('server listening', { name: bootstrap.server.name, url: bootstrap.server.url });
});
