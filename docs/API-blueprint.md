FORMAT: 1A
HOST: https://bus.claroviajes.com/content/v1

# Claroviajes Content

This is the claroviajes-content API specification. The pourpose of this API
is to provide endpoint for other systems to query all types of content managed
by Claroviajes, some of these being: Hotels, Offers, Flights, Prizes.

**Security**

Access to the API is controlled by static keys that must be included in every http
request in the header "apikey". The following is an example of such a header:

```
"apikey": "49le90nWt9Vtr7Z4Voi1nnlEhrhG25u6"
```

The key above is only an example and won't work on the production environment. To
obtain a valid key, please request it to the SysAdmin.

**Production URL**

```
http://bus.claroviajes.com/content/v1
```

**Changelog - Revision 0**

- pois endpoints

**Changelog - Revision 1**

- hotels endpoints

**Changelog - Revision 2**

- flights endpoints

## Available POI Type Values

- hotel
- venue
- airport
- country
- state
- district
- attraction

## Available Property Type Values

- hotel
- bandb
- guest_house
- apartment
- boat

## Available Facility Type Values

- paid_parking
- restaurant
- pets_allowed
- meeting_facilities
- bar
- 24_hour_front_desk
- tennis_court
- sauna
- fitness_room
- golf_course_close_by
- garden
- non_smoking_rooms
- airport_shuttle
- fishing
- business_center
- babysitting_child_services
- facilities_for_disabled
- family_rooms
- casino
- free_parking
- internet_services
- elevator
- solarium
- valet_parking
- spa_wellness_centre
- massage
- vchildren_play_ground
- karaoke
- windsurfing
- hot_tub
- shoe_shine
- canoeing
- hiking
- bbq_facilities
- bowling
- turkish_bath
- diving
- horse_riding
- squash
- luggage_storage
- wireless_lan
- minigolf
- ski_storage
- ski_school
- swimmingpool_indoor
- swimmingpool_outdoor
- free_wifi_internet_access_included
- all_public_and_private_spaces_non_smoking
- private_beach_area
- restaurant_a_la_carte)
- restaurant_buffet
- outdoor_swimming_pool_all_year
- outdoor_swimming_pool_seasonal
- indoor_swimming_pool_all_year
- indoor_swimming_pool_seasonal
- bikes_available_for_free
- nightclub_dj
- ski_to_door_access
- trouser_press
- water_sports_facilities_on_site
- hot_spring_bath
- airport_shuttle_free
- airport_shuttle_with_surcharge
- kids_club
- beach_front
- evening_entertainment
- aqua_park
- daily_maid_service
- grocery_deliveries
- parking_on_site
- private_parking
- different_parking_types
- wifi_everywhere
- paid_wifi

# Group POIs

"POI" stands for Point Of Interest. These POIs represents all the places ("destinies")
that a customer has available for consumption. They may include: cities, districts,
airports or even specific locations like a tourist attraction. These group of endpoints
are commonly used for filtering other types of content, for example: to obtain all hotels
in a specific city.

## POI Collection [/pois]

### Get POIs [GET]

Allows the consumer to query available POIs filtering by the specified parameters.

+ Parameters

  + hint: `new york` (string, required) - A text fragment used to filter the POIs by name.
  + parent: `561dc82c73e967130f030516` (string, optional) - The POI id of the geo-parent location to return only the corresponding children
  + type: `venue` (string, optional) - Comma-separated list of the types of POIs to be returned, see appendix for available values
  + language: `es` (string, optional) - The language ISO code used to display the results of the query.
  + limit: `10` (number, optional) - The max quantity of results to return.
  + page: `1` (number, optional) The page number to slice the results. The value of param 'limit' is used to define the size of the page.
  + format: `raw` (string, optional) A key to specify the projection of the results.

+ Response 200 (application/json)

            [{
                "_id": "561dc82c73e967130f030516",
                "name": "York",
                "language": "es",
                "country": "gb",
                "coords": {
                  "lat": 53.95830154418945,
                  "lng": -1.0818099975585938
                },
                "stats": {
                  "hotelCount": 193
                },
                "handles": {
                  "bookingcom": "-2612321"
                }
              },
              {
                "_id": "561dcb6873e967130f036a97",
                "name": "York",
                "language": "es",
                "country": "au",
                "coords": {
                  "lat": -31.899999618530273,
                  "lng": 116.76699829101562
                },
                "stats": {
                  "hotelCount": 12
                },
                "handles": {
                  "bookingcom": "-1612080"
                }
              },
              {
                "_id": "561dd28973e967130f03d462",
                "name": "Yorkton",
                "language": "es",
                "country": "ca",
                "coords": {
                  "lat": 51.21670150756836,
                  "lng": -102.46700286865234
                },
                "stats": {
                  "hotelCount": 9
                },
                "handles": {
                  "bookingcom": "-576314"
                }
              }]


# Group Hotels

These endpoints provide access to a repository of hotels available for on-demand
booking. The consumer will be able to browse static data about a hotel (location,
description, photos, etc) and also dynamic data regarding the availability the
hotel for specific dates and room quantity.

## Hotels Collection [/hotels]

### Search Hotels Without Availability [GET]

Allows the consumer to query available hotels in a specific location, without specifying
a booking itinerary (dates and quantity). The results will contain only static hotel data.

+ Parameters

  + poi: `561dc82c73e967130f030516` (string, required) - The Id of the POI used to filter the results of hotels belonging to that location.
  + language: `es` (string, optional) - The language ISO code used to display the results of the query.
  + properties: 'bandb,appartment' (string, optional) - Comma separated list of property types to filter the hotels, see appendix for available values.
  + minPrice: `200` (number, optional) - The minimum price per night to filter the hotels
  + maxPrice: `500` (number, optional) - The maximum price per night to filter the hotels
  + minScore: `3` (number, optional) - The minimum score (rating) to filter the hotels
  + maxScore: `7` (number, optional) - The maximum score (rating) to filter the hotels
  + minStars: `2` (number, optional) - The minimum stars (class) to filter the hotels
  + maxStars: `4` (number, optional) - The maximum stars (class) to filter the hotels
  + facilities: 'pool,wifi' (string, optional) - Comma separated list of facilities to filter the hotels, see appendix for available values.
  + limit: `10` (number, optional) - The max quantity of results to return.
  + page: `1` (number, optional) The page number to slice the results. The value of param 'limit' is used to define the size of the page.
  + format: `raw` (string, optional) A key to specify the projection of the results.

+ Response 200 (application/json)

      [{
        "_id": "561defc30196fa0100bea628",
        "name": "Poppies Hotel",
        "slug": "poppies",
        "stars": 3,
        "type": "Hotel",
        "roomCount": 9,
        "country": "gb",
        "status": "public",
        "__v": 0,
        "workflow": {
          "type": "bookingcache",
          "updatedOn": "2015-10-14T06:01:39.864Z",
          "createdOn": "2015-10-14T06:01:39.864Z"
        },
        "rooms": [],
        "facilities": [],
        "photos": [],
        "statistics": {
          "reviewCount": 129,
          "reviewScore": 8.8
        },
        "rates": {
          "min": 79,
          "max": 105,
          "currency": "GBP"
        },
        "location": {
          "streetAddress": "Leny Road",
          "zipCode": "FK17 8AL",
          "city": {
            "poiId": "561dc87a73e967130f0314cd",
            "name": "Callander"
          },
          "coords": {
            "lat": 56.24507429465241,
            "lng": -4.224457740783691
          }
        },
        "languages": [
          "en"
        ],
        "handles": {
          "bookingcom": "34535"
        },
        "availability": {
          "currency": "GBP",
          "totalRate": {
            "min": "160.00",
            "max": "190.00"
          },
          "perNightRate": {
            "min": "80.00",
            "max": "95.00"
          }
        },
        "mainPhoto": {
          "url_original": "http://aff.bstatic.com/images/hotel/max500/137/13779354.jpg",
          "photo_id": "13779354",
          "url_max300": "http://aff.bstatic.com/images/hotel/max300/137/13779354.jpg",
          "url_square60": "http://aff.bstatic.com/images/hotel/square60/137/13779354.jpg"
        }
      },
      {
        "_id": "561defeb0196fa0100beaa1a",
        "name": "The Crags Hotel",
        "slug": "the-crags",
        "stars": 3,
        "type": "Hotel",
        "roomCount": 8,
        "country": "gb",
        "status": "public",
        "__v": 0,
        "workflow": {
          "type": "bookingcache",
          "updatedOn": "2015-10-14T06:02:19.690Z",
          "createdOn": "2015-10-14T06:02:19.690Z"
        },
        "rooms": [],
        "facilities": [],
        "photos": [],
        "statistics": {
          "reviewCount": 230,
          "reviewScore": 7.5
        },
        "rates": {
          "min": 60,
          "max": 120,
          "currency": "GBP"
        },
        "location": {
          "streetAddress": "101 Main St",
          "zipCode": "FK17 8BQ",
          "city": {
            "poiId": "561dc87a73e967130f0314cd",
            "name": "Callander"
          },
          "coords": {
            "lat": 56.243081648587136,
            "lng": -4.212068617343903
          }
        },
        "languages": [
          "en"
        ],
        "handles": {
          "bookingcom": "36239"
        },
        "availability": {
          "currency": "GBP",
          "totalRate": {
            "min": "120.00",
            "max": "240.00"
          },
          "perNightRate": {
            "min": "60.00",
            "max": "120.00"
          }
        },
        "mainPhoto": {
          "url_original": "http://aff.bstatic.com/images/hotel/max500/237/2377708.jpg",
          "photo_id": "2377708",
          "url_max300": "http://aff.bstatic.com/images/hotel/max300/237/2377708.jpg",
          "url_square60": "http://aff.bstatic.com/images/hotel/square60/237/2377708.jpg"
        }
      }]

### Search Hotels With Availability [GET]

Allows the consumer to query available hotels in a specific location, specifying
a booking itinerary (dates and quantity). The results will contain the static hotel data
and an optional extra member detailing the "availability" for the itinerary. If there is NO
availability for a hotel in the specified dates, the corresponding data member will be null.

+ Parameters

  + poi: `561dc82c73e967130f030516` (string, required) - The Id of the POI used to filter the results of hotels belonging to that location.
  + arrival: `2016-02-15` (string, required) - The date in which the customer wants to checkin into the hotel. It should be formatted like YYYY-MM-DD.
  + departure: `2016-02-15` (string, required) - The date in which the customer wants to checkout of the hotel. It should be formatted like YYYY-MM-DD.
  + room1: `2,5,8,A,A` (string, required) - comma separated values of the ages of each children or the character 'A' for adults that will occupy room 1.
  + roomN: `2,5,8,A,A` (string, required) - The same structure as room1 param but for occupants of room N.
  + language: `es` (string, optional) - The language ISO code used to display the results of the query.
  + properties: 'bandb,appartment' (string, optional) - Comma separated list of property types to filter the hotels, see appendix for available values.
  + minPrice: `200` (number, optional) - The minimum price per night to filter the hotels
  + maxPrice: `500` (number, optional) - The maximum price per night to filter the hotels
  + minScore: `3` (number, optional) - The minimum score (rating) to filter the hotels
  + maxScore: `7` (number, optional) - The maximum score (rating) to filter the hotels
  + minStars: `2` (number, optional) - The minimum stars (class) to filter the hotels
  + maxStars: `4` (number, optional) - The maximum stars (class) to filter the hotels
  + facilities: 'pool,wifi' (string, optional) - Comma separated list of facilities to filter the hotels, see appendix for available values.
  + limit: `10` (number, optional) - The max quantity of results to return.
  + page: `1` (number, optional) The page number to slice the results. The value of param 'limit' is used to define the size of the page.
  + format: `raw` (string, optional) A key to specify the projection of the results.

+ Response 200 (application/json)

      [{
        "_id": "561defc30196fa0100bea628",
        "name": "Poppies Hotel",
        "slug": "poppies",
        "stars": 3,
        "type": "Hotel",
        "roomCount": 9,
        "country": "gb",
        "status": "public",
        "__v": 0,
        "workflow": {
          "type": "bookingcache",
          "updatedOn": "2015-10-14T06:01:39.864Z",
          "createdOn": "2015-10-14T06:01:39.864Z"
        },
        "rooms": [],
        "facilities": [],
        "photos": [],
        "statistics": {
          "reviewCount": 129,
          "reviewScore": 8.8
        },
        "rates": {
          "min": 79,
          "max": 105,
          "currency": "GBP"
        },
        "location": {
          "streetAddress": "Leny Road",
          "zipCode": "FK17 8AL",
          "city": {
            "poiId": "561dc87a73e967130f0314cd",
            "name": "Callander"
          },
          "coords": {
            "lat": 56.24507429465241,
            "lng": -4.224457740783691
          }
        },
        "languages": [
          "en"
        ],
        "handles": {
          "bookingcom": "34535"
        },
        "availability": {
          "currency": "GBP",
          "totalRate": {
            "min": "160.00",
            "max": "190.00"
          },
          "perNightRate": {
            "min": "80.00",
            "max": "95.00"
          }
        },
        "mainPhoto": {
          "url_original": "http://aff.bstatic.com/images/hotel/max500/137/13779354.jpg",
          "photo_id": "13779354",
          "url_max300": "http://aff.bstatic.com/images/hotel/max300/137/13779354.jpg",
          "url_square60": "http://aff.bstatic.com/images/hotel/square60/137/13779354.jpg"
        }
      },
      {
        "_id": "561defeb0196fa0100beaa1a",
        "name": "The Crags Hotel",
        "slug": "the-crags",
        "stars": 3,
        "type": "Hotel",
        "roomCount": 8,
        "country": "gb",
        "status": "public",
        "__v": 0,
        "workflow": {
          "type": "bookingcache",
          "updatedOn": "2015-10-14T06:02:19.690Z",
          "createdOn": "2015-10-14T06:02:19.690Z"
        },
        "rooms": [],
        "facilities": [],
        "photos": [],
        "statistics": {
          "reviewCount": 230,
          "reviewScore": 7.5
        },
        "rates": {
          "min": 60,
          "max": 120,
          "currency": "GBP"
        },
        "location": {
          "streetAddress": "101 Main St",
          "zipCode": "FK17 8BQ",
          "city": {
            "poiId": "561dc87a73e967130f0314cd",
            "name": "Callander"
          },
          "coords": {
            "lat": 56.243081648587136,
            "lng": -4.212068617343903
          }
        },
        "languages": [
          "en"
        ],
        "handles": {
          "bookingcom": "36239"
        },
        "availability": {
          "currency": "GBP",
          "totalRate": {
            "min": "120.00",
            "max": "240.00"
          },
          "perNightRate": {
            "min": "60.00",
            "max": "120.00"
          }
        },
        "mainPhoto": {
          "url_original": "http://aff.bstatic.com/images/hotel/max500/237/2377708.jpg",
          "photo_id": "2377708",
          "url_max300": "http://aff.bstatic.com/images/hotel/max300/237/2377708.jpg",
          "url_square60": "http://aff.bstatic.com/images/hotel/square60/237/2377708.jpg"
        }
      }]

## Hotel Detail [/hotels/:hotelId]

### Get Hotel Detail With Availability [GET]

Allows the consumer to query detailed information about a single hotel, specifying
a booking itinerary (dates and quantity). The results will contain the static hotel data
and an optional extra member detailing the "availability" for the itinerary. If there is NO
availability for a hotel in the specified dates, the corresponding data member will be null.

+ Parameters

  + hotelId: `561dc82c73e967130f030516` (string, required) - The Id of the Hotel to retrieve the detail.
  + arrival: `2016-02-15` (string, required) - The date in which the customer wants to checkin into the hotel. It should be formatted like YYYY-MM-DD.
  + departure: `2016-02-15` (string, required) - The date in which the customer wants to checkout of the hotel. It should be formatted like YYYY-MM-DD.
  + language: `es` (string, optional) - The language ISO code used to display the results of the query.
  + format: `raw` (string, optional) A key to specify the projection of the results.

+ Response 200 (application/json)

      {
        "_id": "561defc30196fa0100bea628",
        "name": "Poppies Hotel",
        "slug": "poppies",
        "stars": 3,
        "type": "Hotel",
        "roomCount": 9,
        "country": "gb",
        "status": "public",
        "workflow": {
          "type": "bookingcache",
          "updatedOn": "2015-10-14T06:01:39.864Z",
          "createdOn": "2015-10-14T06:01:39.864Z"
        },
        "rooms": [],
        "facilities": [
          "12",
          "19",
          "69",
          "70",
          "76",
          "86"
        ],
        "photos": [],
        "statistics": {
          "reviewCount": 129,
          "reviewScore": 8.8
        },
        "rates": {
          "min": 79,
          "max": 105,
          "currency": "GBP"
        },
        "location": {
          "streetAddress": "Leny Road",
          "zipCode": "FK17 8AL",
          "city": {
            "poiId": "561dc87a73e967130f0314cd",
            "name": "Callander"
          },
          "coords": {
            "lat": 56.24507429465241,
            "lng": -4.224457740783691
          }
        },
        "languages": [
          "en"
        ],
        "handles": {
          "bookingcom": "34535"
        },
        "availability": [
          {
            "photos": [
              {
                "url_original": "http://aff.bstatic.com/images/hotel/max500/129/1298096.jpg",
                "photo_id": "1298096",
                "url_max300": "http://aff.bstatic.com/images/hotel/max300/129/1298096.jpg",
                "url_square60": "http://aff.bstatic.com/images/hotel/square60/129/1298096.jpg"
              },
              {
                "url_original": "http://aff.bstatic.com/images/hotel/max500/248/24809798.jpg",
                "photo_id": "24809798",
                "url_max300": "http://aff.bstatic.com/images/hotel/max300/248/24809798.jpg",
                "url_square60": "http://aff.bstatic.com/images/hotel/square60/248/24809798.jpg"
              }
            ],
            "max_occupancy": "2",
            "refundable_until": "2015-10-31 23:59:59 +0000",
            "breakfast_included": "1",
            "name": "Standard Double Room",
            "min_price": {
              "currency": "GBP",
              "price": "160.00"
            },
            "deposit_required": 0,
            "block_id": "3453502_87878828_0_1",
            "incremental_price": [
              {
                "currency": "GBP",
                "price": "160.00"
              },
              {
                "currency": "GBP",
                "price": "320.00"
              },
              {
                "currency": "GBP",
                "price": "480.00"
              }
            ],
            "rack_rate": {
              "currency": "GBP",
              "price": "0.00"
            },
            "refundable": "1"
          }
        ],
        "mainPhoto": {
          "url_original": "http://aff.bstatic.com/images/hotel/max500/137/13779354.jpg",
          "photo_id": "13779354",
          "hotel_id": "34535",
          "url_max300": "http://aff.bstatic.com/images/hotel/max300/137/13779354.jpg",
          "url_square60": "http://aff.bstatic.com/images/hotel/square60/137/13779354.jpg"
        }
      }

# Group Zones
These endpoints provide access to Zones of Falcon Api. Is necesary for consumer /flights endpoint.
Return IATA codes for /flights endpoint.

## Zones Collection [/pois/iatas]

### Search Flight Zones [GET]

    + Parameters

      + hint: `Miami` (string, required) - Destination you are looking

    + Response 200 (application/json)

        [{
          "code": "10024",
          "name": "Miami Aeropuerto Internacional",
          "iata": null
          }, {
          "code": "1208",
          "name": "Miami, Florida, Usa",
          "iata": "MIA"
        }]


# Group Flights

These endpoints provide access to Falcon Api. The consumer will be able to browse static data about a flight (airline,description, stops, arrivals and outgos, etc).

## Flights Collection [/flights]

### Search Availability Flights [GET]

Allows the consumer to query available flights in a specific location, specifying
a departure and arrival value or departure only. The results will contain an array of flights data.

  + Parameters

      + itinerary: `BUE,MDZ,2015-09-01;MDZ,BUE,2015-09-08` (string, required) - Departure and arrival IATA code with dates. Include ';' for determinate flight type: Oneway or Roundtrip.
      + paxs: `adults: 5, children: [4, 4, 5], babies: [1, 1]` (string, required) - Pax configuration. The first value is adults quantity. The second value is an array with the specific ages of children. The third value is an array with the with the specific ages of babies.
      + cabin_type: '1' (number, optional) - The cabin type: 1 = Business. 2 = First class. 3 = Economic class.


  + Response 200 (application/json)

          [{
            "airItinerary":[
              outGo: {
                "flightSegments":[
                  {
                    "departureAirport":{
                      "locationCode":"AEP",
                      "locationName":"Jorge Newbery Aeropuerto - Buenos Aires"
                    },
                    "arrivalAirport":{
                      "locationCode":"CWB",
                      "locationName":"Curitiba - Parana - Sur Region"
                    },
                    "operatingAirline":{
                      "code":"AU",
                      "name":"Aerolineas Argentinas",
                      "url":"\/\/desarrollo-asatej-com.s3.amazonaws.com\/prestador-AR%20logo.gif"
                    },
                    "equipment":{
                      "airEquipType":"E90"
                    },
                    "marketingAirline":{
                      "code":"AR",
                      "name":"Aerolineas Argentinas",
                      "url":"\/\/desarrollo-asatej-com.s3.amazonaws.com\/prestador-AR%20logo.gif"
                    },
                    "marriageGrp":"RP",
                    "departureDateTime":"2015-12-16T12:05:00",
                    "arrivalDateTime":"2015-12-16T15:17:00",
                    "stopQuantity":"0",
                    "rph":"1",
                    "flightNumber":"2266",
                    "status":"AV",
                    "durationStep":"2h 12m",
                    "acumulateDuration":"2h 12m"
                  }],
                "refNumber":"MO8Xdq5ndlCEA60sIGSbamggKNUorcalgSE",
                "totalFlightDuration":"26h 20m"
              },
              return: {
                "flightSegments":[
                  {
                    "departureAirport":{
                      "locationCode":"DXB",
                      "terminal":"1",
                      "locationName":"Dubai Aeropuerto Internacional - Dubai City - Dubai"
                    },
                    "arrivalAirport":{
                      "locationCode":"LHR",
                      "terminal":"5",
                      "locationName":"Heathrow Aeropuerto - Londres - Inglaterra"
                    },
                    "operatingAirline":{
                      "code":"BA",
                      "name":"BRITISH AIRWAYS",
                      "url":"\/\/desarrollo-asatej-com.s3.amazonaws.com\/prestador-BA%20logo.gif"
                    },
                    "equipment":{
                      "airEquipType":"777"
                    },
                    "marketingAirline":{
                      "code":"BA",
                      "name":"BRITISH AIRWAYS",
                      "url":"\/\/desarrollo-asatej-com.s3.amazonaws.com\/prestador-BA%20logo.gif"
                    },
                    "marriageGrp":"RP",
                    "departureDateTime":"2015-12-18T15:10:00",
                    "arrivalDateTime":"2015-12-18T19:15:00",
                    "stopQuantity":"0",
                    "rph":"4",
                    "flightNumber":"104",
                    "status":"AV",
                    "durationStep":"8h 5m",
                    "acumulateDuration":"8h 5m"
                  }
                ],
                "refNumber":"MO8Xdq5ndlCEA60sIGSbamggKNUorcalgSEmNO+VUlgX2",
                "totalFlightDuration":"26h 40m"
              }
            ],
            "airItineraryPricingInfo":{
              "itinTotalFare":{
                "totalFare":{
                  "amount":"158896.29",
                  "currencyCode":"ARS",
                  "decimalPlaces":"2"
                },
                "tpsExtensions":{
                  "idVuelo":"PUB@@@Amad@@@AR2266YG31931YEK262Y-BA104YBA247YG37680Y-@@@"
                },
                "totalNeto":{
                  "amount":"158896.29",
                  "currencyCode":"ARS",
                  "decimalPlaces":2
                }
              },
              "ptcFareBreakdowns":{
                "ptcFareBreakdown":[
                  {
                    "passengerTypeQuantity":{
                      "code":"ADT",
                      "Quantity":"1"
                    },

                    "passengerFare":{
                      "baseFare":{
                        "amount":"111121.89",
                        "currencyCode":"ARS",
                        "decimalPlaces":"2"
                      },
                      "taxes":{
                        "tax":{
                          "amount":47774.4,
                          "currencyCode":"ARS",
                          "taxCode":"Others"
                        }
                      },
                      "totalFare":{
                        "amount":"158896.29",
                        "currencyCode":"ARS",
                        "decimalPlaces":"2"
                      }
                    }
                  }
                ]
              }
            },
            "directionInd":"Roundtrip",
            "sourceInfo":"JP",
            "sequenceNmbr":"p\/kvj7ObkOj+waZW9p+hVC5Ro7uY0DfE5SoVrIGOI+8="
          }
        ]
