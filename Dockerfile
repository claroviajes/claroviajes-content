FROM node:0.12

RUN apt-get update
RUN apt-get install --assume-yes libkrb5-dev

ADD ./package.json /app/package.json

WORKDIR /app

RUN npm install bookingcom-client@latest --registry=http://npm.line64.com
RUN npm install claroviajes-pois-client@latest --registry=http://npm.line64.com
RUN npm install claroviajes-hotels-client@latest --registry=http://npm.line64.com
RUN npm install claroviajes-offers-client@latest --registry=http://npm.line64.com
RUN npm install --production

ENV NODE_ENV=production
ENV PORT=8080

ADD ./dist /app/dist

EXPOSE 8080

CMD [ "node", "dist/app.js" ]
